#import "template/offsec_exam_report_template.typ" : *

#let document_title = "OffSec Incident Responder"
#let document_subtitle = "Exam Report"
#let student_email = "student@offsec.com"
#let student_osid = "1337"
#let student_name = "Joe"

//Set document metadata
#set document(
    title: document_title + " - " + document_subtitle,
    author: student_email + ", OS-" + student_osid
)

//Load the template
#show: doc => offsec_exam_report(
    title: document_title,
    subtitle: document_subtitle,
    header_title: "OSIR Exam Report | OS-" + student_osid,
    osid: student_osid,
    email: student_email,
    date: datetime(month: 1, day: 1, year: 2024),
    titlepage_color: rgb("#0ba277"),
    link_color: rgb("#0ba277"),
    logo: "./logos/IR-200.svg",
    logo_size: 80pt,
    body: doc
)

//Document body
#numbered_heading[= #document_subtitle]
#numbered_heading[== Introduction]
The #document_title exam report contains contains all efforts
that were conducted in order to pass the OffSec certification examination.
This report should contain all items that were used to pass the exam and it
will be graded from a standpoint of correctness and fullness to all aspects
of the exam. The purpose of this report is to ensure that the student has a
full understanding of threat hunting methodologies as well as the technical
knowledge to pass the qualifications for the OffSec Incident Responder.


#numbered_heading[== Objective]
The objective of this assessment is to respond to an incident in the Megacorp One environment.
The objective in the first phase is to identify all compromised systems and detect if sensitive data was exfiltrated or encrypted.
Phase 2 involves performing a forensic analysis on a disk image provided by a colleague from another branch of the Megacorp One's Incident Response team.
Based on their initial analysis, the disk image contains a post-exploitation framework binary that contains an encryption key.
You must find the binary and obtain the encryption key.

Example pages have already been created for you at the latter portions of this document that should demonstrate the amount of information and detail that is expected in the exam report.
Use the sample report as a guideline to get you through the reporting.

#numbered_heading[== Requirements]
The student will be required to fill out this incident response report fully and to include the following sections:

- Executive Summary (All sections)
- Incident Detection and Identification
    - In this section, provide a detailed, story-style walkthrough of Phase 1. Focus on how you identified the answer to each exercise question, and ensure you include the exact Splunk query used in your investigation.
- Incident Detection and Identification - Containment, Eradication, and Recovery
    - In this section, outline the key steps that can be taken to contain and recover compromised systems, as well as eliminate the threat identified in Phase 1. Focus on actions that mitigate the immediate risk, restore system integrity, and remove any remaining traces of the compromise.
- Incident Detection and Identification - Findings
    - In this section, which contains a timetable of your findings, make sure to document the *overall attacker activity or phase* related to each exercise question.
- Forensic Analysis - Disk Image Analysis
    - In this section, provide a detailed, story-style walkthrough of the disk image analysis in Phase 2), focusing on how you identified the malicious binary.
- Forensic Analysis - Malware Analysis
    - In this section, provide a detailed, story-style walkthrough of the malware analysis process in Phase 2), focusing on how you analyzed and identified the encryption key used by the binary.
- Conclusion

The walkthroughs in the *Incident Detection and Identification*, *Disk Image Analysis*, and *Malware Analysis* sections should be clear and thorough, containing enough explanations and screenshots to allow a technically proficient reader to replicate each step. Additionally, ensure that your workflow and decision-making process throughout the analysis are well explained and easily understood.

#pagebreak()

//--- End of Introduction ---------------------------------

#numbered_heading[= High-Level Summary]

#numbered_heading[== Incident Detection and Identification Overview]
#lorem(200)

#numbered_heading[== High-Level Attack Path]
#lorem(200)

#numbered_heading[== Forensic Analysis Overview]
#lorem(200)

#pagebreak()

#numbered_heading[= Incident Detection and Identification]

#numbered_heading[== Detection]

The SOC team escalated several triggered alerts to the Incident Response team as shown in the following screenshot.

#picture("template/sample_data/sample.png", "X")

One of the escalated alerts is named “Malicious Apps” and monitors the collected events for occurrences of SHA-256 hashes of known malicious applications that are commonly used by threat actors such as Mimikatz and NetExec.
The alert triggered only one time for an event recorded at 01/11/2024 1:11:11 AM.

Since events containing information about the usage of applications that are commonly used by attackers may have severe implications, let’s review this event in more detail.

#picture("template/sample_data/sample.png", "X")

The event provides us several important information that can be leveraged in our incident detection and identification process:

- Username: Administrator
- Filename: malicious.exe
- Directory: `C:\malicious\`

Based on the matching SHA-256 hash and the characteristic commandline argument “sekurlsa::logonpasswords”, we can be certain that this is Mimikatz.
...

#numbered_heading[== Containment, Eradication, and Recovery]
Once the compromise of PC1, PC2, and SRV1 was confirmed, immediate containment measures were implemented.
The affected machines were isolated from the network to prevent further lateral movement and to cut off communication with the attacker’s command and control (C&C) infrastructure.

The eradication phase began with a thorough audit of all local Administrator accounts across the environment to ensure that passwords were unique and secure.
The shared local Administrator password between PC1 and PC2 was replaced with new, secure passwords on both machines.
Other systems using the same original password were identified and remediated as well.
User accounts that had cached credentials on PC2, likely extracted by the attacker using Mimikatz, were reset.
Clean backups were restored to PC1, PC2, and SRV1 to remove any residual threats, and their local Administrator account passwords were updated.
To further strengthen security, all local Administrator and user account passwords were reset, and a strong password policy, alongside account lockout mechanisms, was enforced to prevent future password attacks.

#picture("template/sample_data/sample.png", "X")

#numbered_heading[== Findings]

#text(size: 8pt,[
#table(
    align: left,
    stroke: .5pt + black,
    columns: (1fr, 2fr, 1fr),
    table.header([*Timestamp*], [*Observation*], [*Affected Assets*]),
    [01/01/2024 1:00:00 PM],[Beginning of Password Spraying],[Host: Workstation1],
    [01/01/2024 1:05:00 PM],[Successful login for local Administrator user],[
        Host: Workstation1 \
        User: Administrator (local)
    ],
    [...],[...],[...]
)
])

#pagebreak()

#numbered_heading[= Forensic Analysis]

#numbered_heading[== Disk Image Analysis]
#lorem(200)
#picture("template/sample_data/sample.png", "X")
#lorem(200)
#picture("template/sample_data/sample.png", "X")

#numbered_heading[== Conclusion]

Our incident detection and identification process successfully uncovered three compromised systems within Megacorp One’s infrastructure, alongside the exfiltration of our confidential chocolate muffin recipe.

During forensic analysis, it was revealed that the threat actor downloaded a password-protected archive on one of the compromised systems, extracting a malicious binary.
This binary leveraged temporary tokens to secure an authentication token linked to the threat actor's Command & Control (C&C) infrastructure.
By obtaining this authentication token, we gained valuable insights into the threat actor’s operations, enhancing our ability to defend against future attacks.

Through swift containment, recovery of the compromised systems, and eradication of the malicious artifacts, we successfully mitigated the threat and prevented further compromise of Megacorp One’s environment.

#pagebreak()

//--- Appendices ------------------------------------------

#numbered_heading[= Appendices]

#numbered_heading[== Appendix: IOCs]

Attached is a compiled list of the resulting IOCs found during the incident response activity.

#numbered_heading[=== File Hashes]

#text(size: 8pt,[
#table(
    align: left,
    stroke: .5pt + black,
    columns: (1fr, 3fr),
    table.header([*File Name*], [*SHA256*]),
    [meterpreter.exe],[4ED877F6F154EB6EBB02EE44E4D836C28193D9254A4A3D6AF6236D8F5BAB88D2],
    [mimikatz.exe],[DF99BBABE7BD0E7A1D96CF370B78FDCF250AF380065A3D51F57EDE6A571E2C15],
    [...],[...]
)
])

#numbered_heading[=== Network Communications]

#text(size: 8pt,[
#table(
    align: left,
    stroke: .5pt + black,
    columns: (1fr, 1fr, 1fr),
    table.header([*Type*], [*Value*], [*Description*]),
    [C&C],[192.168.1.1:9999],[meterpreter.exe],
    [Exfiltration],[192.168.1.1:80],[WebDAV Share "loot"],
    [...],[...],[...]
)

])

#numbered_heading[== Appendix: Python code for exploit Y]

Python code sample for exploit Y:

//https://www.exploit-db.com/exploits/49908
#let exploit_2 = read("template/sample_data/sample_exploit_2.py")
#codeblock[#raw(exploit_2, lang: "python")]
