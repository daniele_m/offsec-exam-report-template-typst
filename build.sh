#!/bin/bash

for f in *.typ
do
  o=$(echo $f | sed 's/.typ/.pdf/')
  typst compile $f ./output/$o
done
