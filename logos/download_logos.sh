#!/bin/bash

courses=(
'pen-200'
'pen-300'
'web-200'
'web-300'
'exp-301'
'exp-312'
'exp-401'
'soc-200'
'th-200'
'ir-200'
)

for course in ${courses[@]}; do
    for image in $(curl -sk 'https://www.offsec.com/courses/'$course'/' --compressed | grep -Eo '/_astro/[A-Z]{2,3}.[0-9]{3}[a-zA-Z0-9\.\_\-]{1,30}.svg' | uniq); do
        if [[ $image = *$(echo $course|tr 'a-z' 'A-Z')* ]]; then
            image='https://www.offsec.com'$image
            name=$(echo $image | grep -Eo '[A-Z]{2,3}-[0-9]{3}')
            echo $image' -> '$name'.svg'
            curl -sk $image -o $name'.svg'
            break
        fi
    done
done
