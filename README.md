# OffSec Exam Report Template Typst

<!-- <img src="template/oscp.png" width=250px> -->
<img src="output/osee.png" width=220px>
<img src="output/osmr.png" width=220px>
<img src="output/oswe.png" width=220px>
<img src="output/osed.png" width=220px>
<img src="output/osep.png" width=220px>
<img src="output/osda.png" width=220px>
<img src="output/oswa.png" width=220px>
<img src="output/oscp.png" width=220px>
<img src="output/osth.png" width=220px>
<img src="output/osir.png" width=220px>

\
A simple template for OffSec exam reports, created while exploring [Typst](https://github.com/typst/typst). This tries to be as minimal as possible, but can be easily modified according to your taste. It uses a markdown-like syntax, which allows to be just as fast and efficient in report writing.

Some advantages over Pandoc-LaTeX templates:
- Shorter PDF creation time
- Easy data loading from external files
- Document automation using the builtin scripting language
- Single ~10MB binary and no extra dependencies

## Getting started

### Requirements

The only software requirement is Typst, available as a [binary](https://github.com/typst/typst) or as a [web app](https://typst.app/).

### Importing the template

```typst
#import "template/offsec_exam_report_template.typ" : *
```

The main function in the template is "offsec_exam_report" and can be loaded as follows:

```typst
#show: doc => offsec_exam_report(
    title: "OffSec Certified Professional",
    subtitle: "Exam Report",
    header_title: "OSCP Exam Report | OS-1337",
    osid: "1337",
    email: "student@offsec.com",
    date: datetime(month: 1, day: 1, year: 2024),
    titlepage_color: rgb("#0c8988"),
    link_color: rgb("#0c8988"),
    logo: "logos/PEN-200.svg",
    logo_size: 80pt,
    body: doc
)
```

### Course logos

Course logos are not included but you can use the script "logos/download_logos.sh" to download them from OffSec website.

### Compiling the PDF

You can use the "compile" command for one-shot PDF creation: 

```sh
typst compile oscp.typ oscp.pdf
```

Or the "watch" command to continuously monitor the file and compile it when changes are made:

```sh
typst watch oscp.typ oscp.pdf
```

## Examples

Compiled PDFs can be found in the "output" folder.


## Credits
- [OSCP-Exam-Report.docx](https://www.offsec.com/pwk-online/OSCP-Exam-Report.docx): Official exam report template provided by OffSec
- [noraj/OSCP-Exam-Report-Template-Markdown/](https://github.com/noraj/OSCP-Exam-Report-Template-Markdown/): Markdown Templates for Offensive Security OSCP, OSWE, OSCE, OSEE, OSWP exam report
- [Wandmalfarbe/pandoc-latex-template](https://github.com/Wandmalfarbe/pandoc-latex-template): A pandoc LaTeX template to convert markdown files to PDF or LaTeX

**Course names and logos are propriety of [OffSec](https://www.offsec.com/)**
