#import "template/offsec_exam_report_template.typ" : *

#let document_title = "OffSec Defense Analyst"
#let document_subtitle = "Exam Report"
#let student_email = "student@offsec.com"
#let student_osid = "1337"
#let student_name = "Joe"

//Set document metadata
#set document(
    title: document_title + " - " + document_subtitle,
    author: student_email + ", OS-" + student_osid
)

//Load the template
#show: doc => offsec_exam_report(
    title: document_title,
    subtitle: document_subtitle,
    header_title: "OSDA Exam Report | OS-" + student_osid,
    osid: student_osid,
    email: student_email,
    date: datetime(month: 1, day: 1, year: 2024),
    titlepage_color: rgb("#417695"),
    link_color: rgb("#417695"),
    logo: "./logos/SOC-200.svg",
    logo_size: 80pt,
    body: doc
)

//Document body
#numbered_heading[= #document_subtitle]
#numbered_heading[== Introduction]
The #document_title exam report contains all efforts that were conducted in order
to pass the exam. This report will be graded from a standpoint of correctness and
fullness to all aspects of the exam. The purpose of this report is to ensure that the student has a full
understanding of security detection methodologies as well as the technical knowledge to pass the exam.


#numbered_heading[== Objective]
The objective of this assessment is to perform detections and analysis on the simulated exam network
in order to determine which attacker actions took place in each of the 10 phases.
An example page has already been created for you at the latter portions of this document that should
demonstrate the amount of information and detail that is expected in the exam report. Use the sample
report as a guideline to get you through the reporting.

#numbered_heading[== Requirements]
The student will be required to fill out this exam report fully and to include the following sections:
- Overall High-Level Summary of level of compromise
- Detailed walkthrough of attacker actions in each phase
- Each finding with included screenshots, explanations, event / log entries, and KQL queries if
applicable.

#pagebreak()

//--- End of Introduction ---------------------------------

#numbered_heading[= High-Level Summary]
#lorem(200)

#pagebreak()

#numbered_heading[= Attack Phases]

#numbered_heading[== Phase 1]

#numbered_heading[=== RDP Brute Force]

#lorem(300)

#picture("template/sample_data/sample.png", "X")

#numbered_heading[=== Persistence]

#lorem(300)

#picture("template/sample_data/sample.png", "X")

#numbered_heading[=== Summary]

#lorem(100)

#picture("template/sample_data/sample.png", "X")

#numbered_heading[== Phase 2]
#numbered_heading[== Phase 3]
#numbered_heading[== Phase 4]
#numbered_heading[== Phase 5]
#numbered_heading[== Phase ...]

//--- Appendices ------------------------------------------

#numbered_heading[= Appendices]

#numbered_heading[== Appendix: Ruby code for exploit X]

Ruby code sample for exploit X:

//https://github.com/KINGSABRI/CVE-in-Ruby/blob/master/CVE-2018-7600/cve-2018-7600_exploit.rb
#let exploit_1 = read("template/sample_data/sample_exploit_1.rb")
#codeblock[#raw(exploit_1, lang: "ruby")]

#numbered_heading[== Appendix: Python code for exploit Y]

Python code sample for exploit Y:

//https://www.exploit-db.com/exploits/49908
#let exploit_2 = read("template/sample_data/sample_exploit_2.py")
#codeblock[#raw(exploit_2, lang: "python")]
