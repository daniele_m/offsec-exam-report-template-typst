#import "template/offsec_exam_report_template.typ" : *

#let document_title = "OffSec Web Assessor"
#let document_subtitle = "Exam Report"
#let student_email = "student@offsec.com"
#let student_osid = "1337"
#let student_name = "Joe"

//Set document metadata
#set document(
    title: document_title + " - " + document_subtitle, 
    author: student_email + ", OS-" + student_osid
)

//Load the template
#show: doc => offsec_exam_report(
    title: document_title,
    subtitle: document_subtitle,
    header_title: "OSWA Exam Report | OS-" + student_osid,
    osid: student_osid,
    email: student_email,
    date: datetime(month: 1, day: 1, year: 2024),
    titlepage_color: rgb("#d40051"),
    link_color: rgb("#d40051"),
    logo: "./logos/WEB-200.svg",
    logo_size: 80pt,
    body: doc
)

//Document body
#numbered_heading[= #document_subtitle]
#numbered_heading[== Introduction]
The #document_title exam report contains all efforts that were conducted in order to pass the exam.
This report will be graded from a standpoint of correctness and fullness to all aspects of the exam.
The purpose of this report is to ensure that the student has a the technical knowledge to pass the exam.

#numbered_heading[== Objective]
The objective of this assessment is to perform a black-box penetration test against Offensive Security
Exam network. The student is tasked with following methodical approach in obtaining access to the
objective goals. This test should simulate an actual black-box penetration test with Proof of Concept
and how you would start from beginning to end, including the overall report.

#numbered_heading[== Requirements]
The student will be required to fill out this penetration testing report fully and to include the following
sections:
- Methodology walkthrough and detailed outline of steps taken
- Each finding with included screenshots, walkthrough, sample code, and proof.txt if applicable
- Any additional items that were not included

#pagebreak()

#numbered_heading[= High-Level Summary]
#student_name was tasked with performing a black-box web application penetration test towards Offensive Security Exam network.
A web application penetration test is a dedicated attack against systems that are hosting a web application. The focus of
this test is to perform attacks, similar to those of a hacker and attempt to infiltrate Offensive Security’s
eaxm systems. #student_name’s overall objective was to evaluate web applications security,
identify technologies, and exploit flaws while reporting the findings back to Offensive Security.
When performing the web application penetration test, there were several alarming vulnerabilities that were
identified on Offensive Security’s web apps. When performing the attacks, #student_name was able to gain access
to sensitive information on multiple machines, primarily due to code issues. #text(fill: rgb("#8B0000"))[All systems were successfully
compromised]. These systems as well as a brief description on how access was obtained are listed below:

- #text(fill: rgb("#8B0000"))[HOSTNAME] - Name of initial exploit
- #text(fill: rgb("#8B0000"))[HOSTNAME] - Name of initial exploit
- #text(fill: rgb("#8B0000"))[HOSTNAME] - Name of initial exploit
- #text(fill: rgb("#8B0000"))[HOSTNAME] - Name of initial exploit
- #text(fill: rgb("#8B0000"))[HOSTNAME] - Name of initial exploit

#numbered_heading[== Recommendations]
#student_name recommends patching the vulnerabilities identified during the testing to ensure that an attacker
cannot exploit these systems in the future.
#pagebreak()

//--- End of introduction ---------------------------------

//--- Target 1 ---------------------------------------------

#numbered_heading[= Target 1 -- 192.168.X.X]

#numbered_heading[== Proof of Exploitation]

Interactive shell or Burp request and response. Better if the flag is shown in the browser too.


#picture("template/sample_data/sample_proof.png", "Proof of exploitation")

#numbered_heading[== Vulnerability 1]
#lorem(150)

#numbered_heading[=== Vulnerability Description]

// Get CVSS from CVE information or calculate at https://nvd.nist.gov/vuln-metrics/cvss/v3-calculator
// #severity("Critical") 9.6, CVSS:3.1/AV:A/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:L

#lorem(50)

#numbered_heading[=== Steps to Reproduce]
#lorem(150)
#terminal_window[
    #read("template/sample_data/sample_cmdline_1.log")   
]

#numbered_heading[=== Vulnerability Fix]
#lorem(20)

#numbered_heading[== Vulnerability 2]
#numbered_heading[== Vulnerability 3]
#numbered_heading[== Vulnerability ...]

#pagebreak()

//--- Target 2 ---------------------------------------------

#numbered_heading[= Target 2 -- 192.168.X.X]

#numbered_heading[== Proof of Exploitation]

Interactive shell or Burp request and response. Better if the flag is shown in the browser too.


#picture("template/sample_data/sample_proof.png", "Proof of exploitation")

#numbered_heading[== Vulnerability 1]
#lorem(300)

#numbered_heading[=== Vulnerability Description]

// Get CVSS from CVE information or calculate at https://nvd.nist.gov/vuln-metrics/cvss/v3-calculator
// #severity("High") 8.4, CVSS:3.1/AV:L/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H

#lorem(50)

#numbered_heading[=== Steps to Reproduce]
#lorem(150)
#terminal_window[
    #read("template/sample_data/sample_cmdline_1.log")   
]

#numbered_heading[=== Vulnerability Fix]
#lorem(20)

#numbered_heading[== Vulnerability 2]
#numbered_heading[== Vulnerability 3]
#numbered_heading[== Vulnerability ...]

#pagebreak()

//--- Target 3 ---------------------------------------------

#numbered_heading[= Target 3 -- 192.168.X.X]

#numbered_heading[== Proof of Exploitation]

Interactive shell or Burp request and response. Better if the flag is shown in the browser too.


#picture("template/sample_data/sample_proof.png", "Proof of exploitation")

#numbered_heading[== Vulnerability 1]
#lorem(100)

#numbered_heading[=== Vulnerability Description]

// Get CVSS from CVE information or calculate at https://nvd.nist.gov/vuln-metrics/cvss/v3-calculator
// #severity("Critical") 9.6, CVSS:3.1/AV:A/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:L

#lorem(50)

#numbered_heading[=== Steps to Reproduce]
#lorem(150)
#terminal_window[
    #read("template/sample_data/sample_cmdline_1.log")   
]

#numbered_heading[=== Vulnerability Fix]
#lorem(20)

#numbered_heading[== Vulnerability 2]
#numbered_heading[== Vulnerability 3]
#numbered_heading[== Vulnerability ...]

#pagebreak()

//--- Appendices ------------------------------------------

#numbered_heading[= Appendices]

#numbered_heading[== Appendix: Local and Proof list]

Complete list of local and proof flags:

#table(
    align: center,
    stroke: .5pt + black,
    columns: (1fr, 1fr, 2fr),
    [192.168.X.X],[local.txt],[8891ea8df79a98de4b13ebcfcde7f37c],
    [192.168.X.X],[proof.txt],[8891ea8df79a98de4b13ebcfcde7f37c],
    [192.168.X.X],[local.txt],[8891ea8df79a98de4b13ebcfcde7f37c],
    [192.168.X.X],[proof.txt],[8891ea8df79a98de4b13ebcfcde7f37c],
    [192.168.X.X],[local.txt],[8891ea8df79a98de4b13ebcfcde7f37c],
    [192.168.X.X],[proof.txt],[8891ea8df79a98de4b13ebcfcde7f37c],
    [...],[...],[...],
)

#numbered_heading[== Appendix: Ruby code for exploit X]

Ruby code sample for exploit X:

//https://github.com/KINGSABRI/CVE-in-Ruby/blob/master/CVE-2018-7600/cve-2018-7600_exploit.rb
#let exploit_1 = read("template/sample_data/sample_exploit_1.rb")
#codeblock[#raw(exploit_1, lang: "ruby")]

#numbered_heading[== Appendix: Python code for exploit Y]

Python code sample for exploit Y:

//https://www.exploit-db.com/exploits/49908
#let exploit_2 = read("template/sample_data/sample_exploit_2.py")
#codeblock[#raw(exploit_2, lang: "python")]
