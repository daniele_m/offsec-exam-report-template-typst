#import "template/offsec_exam_report_template.typ" : *

#let document_title = "OffSec Exploitation Expert"
#let document_subtitle = "Exam Report"
#let student_email = "student@offsec.com"
#let student_osid = "1337"
#let student_name = "Joe"

//Set document metadata
#set document(
    title: document_title + " - " + document_subtitle,
    author: student_email + ", OS-" + student_osid
)

//Load the template
#show: doc => offsec_exam_report(
    title: document_title,
    subtitle: document_subtitle,
    header_title: "OSEE Exam Report | OS-" + student_osid,
    osid: student_osid,
    email: student_email,
    date: datetime(month: 1, day: 1, year: 2024),    
    titlepage_color: rgb("#600000"),
    link_color: rgb("#600000"),
    logo: "./logos/EXP-401.svg",
    logo_size: 80pt,
    body: doc
)

//Document body
#numbered_heading[= #document_subtitle]
#numbered_heading[== Introduction]
The #document_title exam report contains all efforts that were conducted in order
to pass the exam. This report will be graded from a standpoint of correctness and
fullness to all aspects of the exam. The purpose of this report is to ensure that the student has a full
understanding of exploit development methodologies as well as the technical knowledge to pass the exam.

#numbered_heading[== Objective]
The Certification Challenge consists of several target machines, accessible over the Exam Labs, that need to be accessed in order to debug any vulnerable software.
The student is tasked with following a methodical approach in analyzing and solving the given assignments.The exam report is meant to be a writeup of the steps taken to solve the assignment, including any analysis performed and code written. An example page has already been created for you at the latter portions of this document that should
give you ample information on what is expected to pass this exam. Use the sample report as a guideline to get you through the reporting, while removing any headlines that are note relevant to a specific assignment.

#numbered_heading[== Requirements]
The student will be required to fill out this penetration testing report fully and to include the following
sections:
- Overall High-Level Summary and Recommendations (non-technical)
- Methodology walkthrough and detailed outline of steps taken
- Each finding with included screenshots, walkthrough, sample code, and proof.txt if applicable
- Any additional items that were not included

#pagebreak()

#numbered_heading[= High-Level Summary]
#student_name was tasked with performing X tasks.

#lorem(200)

During the testing, #student_name was able to complete #text(fill: rgb("#8B0000"))[*X*] out of the #text(fill: rgb("#8B0000"))[*X*] tasks.

#pagebreak()

//--- End of introduction ---------------------------------

//--- Target 1 ---------------------------------------------

#numbered_heading[= Target 1 -- 192.168.X.X]

#numbered_heading[== Proof of Exploitation]


#picture("template/sample_data/sample_proof.png", "Proof of exploitation")

#numbered_heading[== Initial Analysis]
#lorem(300)

#numbered_heading[== Read and Write Primitive]
#lorem(300)

#numbered_heading[== Code Execution]
#lorem(300)

#numbered_heading[== Sandbox Escape]
#lorem(200)

#numbered_heading[== PoC Creation]
#lorem(150)

#terminal_window[
    #read("template/sample_data/sample_cmdline_1.log")   
]

#lorem(150)

#terminal_window[
    #read("template/sample_data/sample_cmdline_2.log")
]
#pagebreak()

//--- Target 2 ---------------------------------------------

#numbered_heading[= Target 2 -- 192.168.X.X]

#numbered_heading[== Proof of Exploitation]


#picture("template/sample_data/sample_proof.png", "Proof of exploitation")

#numbered_heading[== Initial Analysis]
#lorem(300)

#numbered_heading[== Race Condition]
#lorem(300)

#numbered_heading[== Kernel Memory Leak]
#lorem(300)

#numbered_heading[== Read and Write Primitive]
#lorem(300)

#numbered_heading[== Privilege Escalation]
#lorem(300)

#numbered_heading[== PoC Creation]
#lorem(150)

#terminal_window[
    #read("template/sample_data/sample_cmdline_1.log")   
]

#lorem(100)

#terminal_window[
    #read("template/sample_data/sample_cmdline_2.log")
]

#pagebreak()

//--- Appendices ------------------------------------------

#numbered_heading[= Appendices]

#numbered_heading[== Appendix: Local and Proof list]

Complete list of local and proof flags:

#table(
    align: center,
    stroke: .5pt + black,
    columns: (1fr, 1fr, 2fr),
    [192.168.X.X],[proof.txt],[8891ea8df79a98de4b13ebcfcde7f37c],
    [...],[...],[...]
)

#numbered_heading[== Appendix: Ruby code for exploit X]

Ruby code sample for exploit X:

//https://github.com/KINGSABRI/CVE-in-Ruby/blob/master/CVE-2018-7600/cve-2018-7600_exploit.rb
#let exploit_1 = read("template/sample_data/sample_exploit_1.rb")
#codeblock[#raw(exploit_1, lang: "ruby")]

#numbered_heading[== Appendix: Python code for exploit Y]

Python code sample for exploit Y:

//https://www.exploit-db.com/exploits/49908
#let exploit_2 = read("template/sample_data/sample_exploit_2.py")
#codeblock[#raw(exploit_2, lang: "python")]
