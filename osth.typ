#import "template/offsec_exam_report_template.typ" : *

#let document_title = "OffSec Threat Hunter"
#let document_subtitle = "Exam Report"
#let student_email = "student@offsec.com"
#let student_osid = "1337"
#let student_name = "Joe"

//Set document metadata
#set document(
    title: document_title + " - " + document_subtitle,
    author: student_email + ", OS-" + student_osid
)

//Load the template
#show: doc => offsec_exam_report(
    title: document_title,
    subtitle: document_subtitle,
    header_title: "OSTH Exam Report | OS-" + student_osid,
    osid: student_osid,
    email: student_email,
    date: datetime(month: 1, day: 1, year: 2024),
    titlepage_color: rgb("#593d8f"),
    link_color: rgb("#593d8f"),
    logo: "./logos/TH-200.svg",
    logo_size: 80pt,
    body: doc
)

//Document body
#numbered_heading[= #document_subtitle]
#numbered_heading[== Introduction]
The #document_title exam report contains contains all efforts
that were conducted in order to pass the OffSec certification examination.
This report should contain all items that were used to pass the exam and it
will be graded from a standpoint of correctness and fullness to all aspects
of the exam. The purpose of this report is to ensure that the student has a
full understanding of threat hunting methodologies as well as the technical
knowledge to pass the qualifications for the OffSec Threat Hunter.


#numbered_heading[== Objective]
The objective of this assessment is to perform a threat hunting sprint in the simulated exam network
environment. The student is tasked with following a methodical approach to identify all compromised
systems and detect if sensitive data was exfiltrated or encrypted. An example page has already been
created for you at the latter portions of this document that should demonstrate the amount of
information and detail that is expected in the exam report. Use the sample report as a guideline to
get you through the reporting.

#numbered_heading[== Requirements]
The student will be required to fill out this exam report fully and to include the following sections:
- High-Level Summary
- Methodology
- Hunt Narrative
    - A detailed walkthrough of the entire threat hunting sprint.
    - The walkthrough should contain an explanation of all steps, assumptions, and decisions supported by screenshots and Splunk queries if applicable.
    - The walkthrough should be thorough enough that the complete threat hunting sprint can be replicated step-by-step by a technically competent reader.
- Findings
    - A timeline of all key activities related to the attacker’s actions
- Conclusion
- IoC Lists in the Appendix

#pagebreak()

//--- End of Introduction ---------------------------------

#numbered_heading[= High-Level Summary]
#lorem(200)

#pagebreak()

#numbered_heading[= Methodology]
#lorem(200)

#pagebreak()

#numbered_heading[== Hunt Narrative]

#numbered_heading[=== RDP Password Spraying]

#lorem(300)

#picture("template/sample_data/sample.png", "X")

#numbered_heading[=== Privilege Escalation]

#lorem(300)

#picture("template/sample_data/sample.png", "X")

#numbered_heading[=== Data Exfiltration]

#lorem(300)

#picture("template/sample_data/sample.png", "X")

#numbered_heading[== Findings]

#text(size: 8pt,[
#table(
    align: left,
    stroke: .5pt + black,
    columns: (1fr, 2fr, 1fr),
    table.header([*Timestamp*], [*Observation*], [*Affected Assets*]),
    [01/01/2024 1:00:00 PM],[Beginning of Password Spraying],[Host: Workstation1],
    [01/01/2024 1:05:00 PM],[Successful login for local Administrator user],[
        Host: Workstation1 \
        User: Administrator (local)
    ],
    [...],[...],[...]
)
])

#numbered_heading[== Conclusion]

Our threat hunting sprint successfully uncovered X compromised systems within the target environment along with the exfiltration of sensitive data.

Based on our findings and actionable insights, the incident response team can now initiate the necessary steps for incident detection and identification,
containment, and restoration of the compromised systems.

In addition, any policy- or regulation-driven actions can be initiated to ensure compliance and further secure the organization.

For comprehensive guidance on potential remediation steps and enhancing detection capabilities, please refer to the compiled list of IoCs provided in the
Appendix of this report. These IoCs serve as a valuable reference and baseline for improving our organization's overall security resilience against similar
threats in the future.

#pagebreak()

//--- Appendices ------------------------------------------

#numbered_heading[= Appendices]

#numbered_heading[== Appendix: IOCs]

Attached is a compiled list of the resulting  IOCs found during the threat hunting sprint.

#numbered_heading[=== File Hashes]

#text(size: 8pt,[
#table(
    align: left,
    stroke: .5pt + black,
    columns: (1fr, 3fr),
    table.header([*File Name*], [*SHA256*]),
    [meterpreter.exe],[4ED877F6F154EB6EBB02EE44E4D836C28193D9254A4A3D6AF6236D8F5BAB88D2],
    [mimikatz.exe],[DF99BBABE7BD0E7A1D96CF370B78FDCF250AF380065A3D51F57EDE6A571E2C15],
    [...],[...]
)
])

#numbered_heading[=== Network Communications]

#text(size: 8pt,[
#table(
    align: left,
    stroke: .5pt + black,
    columns: (1fr, 1fr, 1fr),
    table.header([*Type*], [*Value*], [*Description*]),
    [C&C],[192.168.1.1:9999],[meterpreter.exe],
    [Exfiltration],[192.168.1.1:80],[WebDAV Share "loot"],
    [...],[...],[...]
)

])

#numbered_heading[== Appendix: Python code for exploit Y]

Python code sample for exploit Y:

//https://www.exploit-db.com/exploits/49908
#let exploit_2 = read("template/sample_data/sample_exploit_2.py")
#codeblock[#raw(exploit_2, lang: "python")]
